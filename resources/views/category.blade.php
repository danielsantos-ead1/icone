@include('header')
<div class="courses_section courses-fullwidth">
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h1>{{ $categoria['st_nomeexibicao'] }}</h1>
                <p>{{ $categoria['st_categoria'] }}</p>
            </div>
            <div class="col-xs-12 col-sm-6" style="text-align: right">
                <img style="max-height: 200px; max-width: 500px; margin: 5px" class="responsive" src="{{ $categoria['st_imagemcategoria'] }}" >
            </div>
        </div><!--end row-->


        <div class="row">

            <div class="col-xs-12 courses_right">
                <div class="courses_right_inner">

                    <div class="courses">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center">Curso</th>
                                <th style="text-align: center">Categoria</th>
                                <th style="width: 200px">Parcelas a partir de</th>
                                <th style="text-align: center">+ Detalhes</th>
                                <th style="text-align: center">Dúvidas?</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categoria['produtos'] as $produto)
                                <tr>
                                    <td>
                                        <ul >
                                            <li class="active">
                                                <a href="/curso/{{ $produto['id_projetopedagogico'] }}/{{ mb_strtolower($produto['st_produto'], 'UTF-8') }}">{{ $produto['st_produto'] }}</a>
                                            </li>
                                            <li>
                                                <a href="/curso/{{ $produto['id_projetopedagogico'] }}/{{ mb_strtolower($produto['st_produto'], 'UTF-8') }}">{{ strip_tags($produto['st_descricao']) }}</a>
                                            </li>
                                        </ul>
                                    </td>
                                    <td style="text-align: center">{{ $produto['st_nomeexibicao'] }}</td>
                                    <td style="text-align: center">R$ {{ number_format($produto['nu_valormensal'], 2, ',', '.') }}</td>
                                    <td style="text-align: center">
                                        <a class="btn btn-info btn-matricule-se" href="/curso/{{ $produto['id_projetopedagogico'] }}/{{ mb_strtolower($produto['st_produto'], 'UTF-8') }}" role="button">Saiba mais</a>
                                    </td>
                                    <td style="text-align: center">
                                        <a class="btn btn-success btn-matricule-se" href="/contato/{{ $produto['id_produto'] }}/mais-informações" role="button">Entre em contato</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div><!--End courses-->

                </div><!-- end courses_right_inner -->
            </div><!-- end courses_right -->

        </div><!-- end row -->
    </div><!-- end container -->
</div><!--end post section-->
@include('footer')