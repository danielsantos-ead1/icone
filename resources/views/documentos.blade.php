
    @include('header')
            
            <div class="container">
                <div class="bloco-principal">
                    <div class="col-md-8">
                        <h1 style="text-align:center; margin-top:20px;margin-bottom:40px;">Documentos da IES</h1>
                        <a class="links-arquivos" href="/arquivos/relacao-de-livros-biblioteca-fisica.pdf">Relação de Livros</a><br><hr>

                        <a class="links-arquivos" href="/arquivos/Pos-Graduacao-em-Desenvolvimento-Historico-Educacional.pdf">Pós-Graduação em Desenvolvimento Histórico</a><br><hr>

                        <a class="links-arquivos" href="/arquivos/Pos-Graduacao-em-Gestao-e-Orientacao-Educacional.pdf">Pós-Graduação em Gestão e Orientação Educacional</a><br><hr>

                        <a class="links-arquivos" href="/arquivos/Pos-Graduacao-em-Neuropsicopedagogia-e-Psicomotricidade.pdf">Pós-Graduação em Neuropsicopedagogia e Psicomotricidade</a><br><hr>

                        <a class="links-arquivos" href="/arquivos/Pos-Graduacao-em-PsicomotrIcidade-Educacao-e-Musicoterapia.pdf">Pós-Graduação em Psicomotricidade, Educação e Musicoterapia</a><br><hr>

                        <a class="links-arquivos" href="/arquivos/Pos-Graduacao-em-Psicopedagogia.pdf">Pós-Graduação em Psicopedagogia</a><br><hr>

                        <a class="links-arquivos" href="/arquivos/Projeto-de-Pos-Graduacao-em-Docencia-do-Ensino-Superior.pdf">Pós-Graduação em Docência do Ensino Superior</a><br><hr>

                        <a class="links-arquivos" href="/arquivos/PDI-FACULDADE-DE-TECNOLOGIA-ICONE.pdf">PDI - Faculdade de Tecnologia Ícone</a><br><hr>

                    </div>

                    <div class="col-xs-12 col-sm-4 col-md-4 custom_left" style="margin-top:40px;">
                        <div class="sidebar">                       

                            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                            
                            <div class="sidebar_item">
                                <div class="item_inner program">
                                    
                                        <h2 class="titulo-form">PREENCHA OS CAMPOS ABAIXO PARA MAIS INFORMAÇÕES.</h2>
                                        <form name="classic" action="#" method="post" class=""> <!-- http://crm.eaducam.com.br/leads/importacao.asp? confirmacao.php-->
                                        
                                            <div class="fields col-esquerda col-12 col-md-6">
                                                <label>ESCOLHA A ÁREA<span class="text-vinho">*</span></label><br>
                                                <select id="categoria" onchange="OnSelectionChange()" class="select-curso" name="tipocurso" required="">
                                                    <option selected=""></option>
                                                    @foreach($categorias as $categoria)
                                                        <option value="{{ $categoria['st_categoria'] }}">{{ $categoria['st_categoria'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>                                    

                                            <div class="fields col-md-6">
                                                <label>ESCOLHA SEU CURSO<span class="text-vinho">*</span></label><br>
                                                <select id="direito" class="select-curso" name="curso">
                                                    <option selected=""></option>                           
                                                    @foreach ($categorias as $item) {
                                                        @foreach ($item['produtos'] as $item2) {
                                                            @if ($item2['id_categoria'] == 101) {
                                                                <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endforeach                                       
                                                </select>

                                                <select id="educacao" class="select-curso" name="curso" hidden>
                                                    <option selected=""></option>
                                                    @foreach ($categorias as $item) {
                                                        @foreach ($item['produtos'] as $item2) {
                                                            @if ($item2['id_categoria'] == 96) {
                                                                <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                </select>

                                                <select id="gestao-publica" class="select-curso" name="curso" hidden>
                                                    <option selected=""></option>
                                                    @foreach ($categorias as $item) {
                                                        @foreach ($item['produtos'] as $item2) {
                                                            @if ($item2['id_categoria'] == 94) {
                                                                <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                </select>

                                                <select id="mba" class="select-curso" name="curso" hidden>
                                                    <option selected=""></option>
                                                    @foreach ($categorias as $item) {
                                                        @foreach ($item['produtos'] as $item2) {
                                                            @if ($item2['id_categoria'] == 100) {
                                                                <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                </select>

                                                <select id="negocios" class="select-curso" name="curso" hidden>
                                                    <option selected=""></option>
                                                    @foreach ($categorias as $item) {
                                                        @foreach ($item['produtos'] as $item2) {
                                                            @if ($item2['id_categoria'] == 98) {
                                                                <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                </select>

                                                <select id="saude" class="select-curso" name="curso" hidden>
                                                    <option selected=""></option>
                                                    @foreach ($categorias as $item) {
                                                        @foreach ($item['produtos'] as $item2) {
                                                            @if ($item2['id_categoria'] == 103) {
                                                                <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                </select>

                                                <select id="seguranca" class="select-curso" name="curso" hidden>
                                                    <option selected=""></option>
                                                    @foreach ($categorias as $item) {
                                                        @foreach ($item['produtos'] as $item2) {
                                                            @if ($item2['id_categoria'] == 97) {
                                                                <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                        

                                            <div class=" fields col-esquerda col-md-6">
                                                <label>NOME<span class="text-vinho">*</span></label><br>
                                                    <input name="firstname" type="text" class="field" required="" placeholder="">
                                            </div>

                                            <div class="fields col-md-6">
                                                <label>SOBRENOME<span class="text-vinho">*</span></label><br>
                                                    <input name="lastname" type="text" class="field" required="" placeholder="">
                                            </div>

                                            <div class="fields col-esquerda col-md-6">
                                                <label>E-MAIL<span class="text-vinho">*</span></label><br>
                                                    <input name="email" type="email" class="field" required="" placeholder="">
                                            </div>

                                            <div class="fields col-md-6">
                                                <label>CELULAR<span class="text-vinho">*</span></label><br>
                                                    <input name="celular" id="phone-cel" type="text" class="field" required="" placeholder="(00) 00000-0000">
                                            </div>

                                            <div class="fields col-esquerda col-md-6">
                                                <label>TELEFONE</label><br>
                                                <input name="telefone" id="phone" type="text" class="field" placeholder="(00) 0000-0000" maxlength="14">
                                            </div>

                                            <div class="fields col-md-6">
                                                <label>UF<span class="text-vinho">*</span></label><br>
                                                    <select id="uf" name="flag1">
                                                        <option selected=""></option>
                                                        <option value="AC">AC</option>
                                                        <option value="AL">AL</option>
                                                        <option value="AP">AP</option>
                                                        <option value="AM">AM</option>
                                                        <option value="BA">BA</option>
                                                        <option value="CE">CE</option>
                                                        <option value="DF">DF</option>
                                                        <option value="ES">ES</option>
                                                        <option value="GO">GO</option>
                                                        <option value="MA">MA</option>
                                                        <option value="MT">MT</option>
                                                        <option value="MS">MS</option>
                                                        <option value="MG">MG</option>
                                                        <option value="PA">PA</option>
                                                        <option value="PB">PB</option>
                                                        <option value="PR">PR</option>
                                                        <option value="PE">PE</option>
                                                        <option value="PI">PI</option>
                                                        <option value="RJ">RJ</option>
                                                        <option value="RN">RN</option>
                                                        <option value="RS">RS</option>
                                                        <option value="RO">RO</option>
                                                        <option value="PR">RR</option>
                                                        <option value="SC">SC</option>
                                                        <option value="SP">SP</option>
                                                        <option value="SE">SE</option>
                                                        <option value="TO">TO</option>
                                                    </select>
                                            </div>
                                                        
                                            <input type="text" name="origem" value="ORIGEM:ALTA_PERFORMANCE" hidden="">
                                            <input type="text" name="abordagem" value="INTERESSE" hidden="">
                                            <input type="text" name="urls" value="http://eaducam.com.br/obrigado" hidden="">
                                    
                                            <div class="botao-enviar-div">
                                                <input type="submit" name="enviar" value="ENVIAR" class="botao-enviar"><br>
                                                <p><span class="text-vinho">*</span>Campos com prenchimento obrigatório</p>

                                            </div>
                                        </form>
                                                            
                                </div>
                            </div>

                            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

                            


                            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->


                            <div class="sidebar_item">
                                <div class="item_inner question">
                                    <h4>Alguma dúvida?</h4>
                                    <h5>Entre em contato:</h5>
                                    @forelse ($site['phones'] as $phone)
                                        <h4 class="phoneNo"><i class="fa fa-phone"></i> {{ $phone }}</h4>
                                    @empty
                                        @if(!empty($site['phone'])) <h4 class="phoneNo"><i class="fa fa-phone"></i> {{ $site['phone'] }}</h4> @endif
                                    @endforelse
                                </div>
                            </div><!--end sidebar item-->


                        </div><!--end sidebar-->

                    </div>
                </div>
            </div>
        
    @include('footer')