<a href="https://api.whatsapp.com/send?phone=5561996268305&amp;text=Ol%C3%A1%21%20%20gostaria%20de%20saber%20mais%20sobre%20os%20cursos%20da%20Facti." class="whatsapp-btn tooltips" data-placement="right" title="" target="blank" data-original-title="Contate o Suporte">
  <i class="fa fa-whatsapp fa-3x"></i>
</a>

<footer class="footer-v1">
  <div class="menuFooter clearfix">
    <div class="container">
      <div class="row clearfix">

        {{--<div class="col-sm-3 col-xs-6">--}}
          {{--<ul class="menuLink">--}}
            {{--<li><a href="about.html">About Royal College</a></li>--}}
            {{--<li><a href="campus.html">About Campus</a></li>--}}
            {{--<li><a href="stuff.html">Staff Members</a></li>--}}
            {{--<li><a href="about.html">Why Choose Us?</a></li>--}}
          {{--</ul>--}}
        {{--</div><!-- col-sm-3 col-xs-6 -->--}}

        <div class="col-sm-4 col-xs-12">
          <ul class="menuLink menu-rodape">
            <li><a href="/" target="_self">Início</a></li>
            <li><a href="/cursos" target="_self">Cursos</a></li>
            <li><a href="/facti" target="_self">FACTI</a></li>
            <li><a href="/entre-em-contato" target="_self">Contato</a></li>
            <li><a href="/ouvidoria" target="_self">Ouvidoria</a></li>
            
          </ul>
        </div><!-- col-sm-3 col-xs-6 -->

        <div class="col-sm-4 col-xs-6 borderLeft">
          <div class="footer-address">
            <h5>Endereço:</h5>
            <address>
            EQNN 03/05, Bloco B, Lotes 1 a 5, 1º andar.<br>
            Ceilândia Norte, Via Leste, DF.<br>
            72.225-535

            </address>
            
            
          </div>
        </div><!-- col-sm-3 col-xs-6 -->

        <div class="col-sm-4 col-xs-6 borderLeft">
          <div class="socialArea">
            <h5>Entre em contato:</h5>
            <ul class="list-inline ">
            @if(!empty($site['social']))
              @if(!empty($site['social']['twitter']))<li><a href="{{ $site['social']['twitter'] }}" target="_blank"><i class="fa fa-twitter"></i></a></li> @endif
              @if(!empty($site['social']['facebook']))<li><a href="{{ $site['social']['facebook'] }}" target="_blank"><i class="fa fa-facebook"></i></a></li> @endif
              @if(!empty($site['social']['instagram']))<li><a href="{{ $site['social']['instagram'] }}" target="_blank"><i class="fa fa-instagram"></i></a></li> @endif              
              @if(!empty($site['social']['gplus']))<li><a href="{{ $site['social']['gplus'] }}" target="_blank"><i class="fa fa-google-plus"></i></a></li> @endif
              @if(!empty($site['social']['youtube'])))<li><a href="{{ $site['social']['youtube'] }}" target="_blank"><i class="fa fa-youtube-play"></i></a></li> @endif
            @endif
            </ul>
          </div><!-- social -->
          
          
          
          <div class="contactNo">
            
            @forelse ($site['phones'] as $phone)
              <h4 class="phoneNo"><i class="fa fa-phone"></i> <span style="font-size:15px;margin-left:9px;">61</span> <span style="font-size:25px;">4141.2662</span></h4>
            @empty
              @if(!empty($site['phone'])) <h4 class="phoneNo"><i class="fa fa-phone"></i> {{ $site['phone'] }}</h4> @endif
            @endforelse
              <h4 class="phoneNo"><i class="fa fa-whatsapp"></i><span style="font-size:15px;margin-left:10px;margin-top:10px;">61</span> <span style="font-size:25px;">9 9626.8305</span></h4>
            
          </div><!-- contactNo -->

          <div>
            <img style="margin-top: 30px;" src="/img/logo-facti3.png" alt="">
          </div>
        </div><!-- col-sm-3 col-xs-6 -->

      </div><!-- row -->
    </div><!-- container -->
  </div><!-- menuFooter -->

  <div class="footer clearfix">
    <div class="container">
      {{--<div class="row clearfix">--}}
        {{--<div class="col-sm-6 col-xs-12 copyRight">--}}
          {{--<p>© 2016 Copyright Royal College Bootstrap Template by <a href="http://www.iamabdus.com">Abdus</a></p>--}}
        {{--</div><!-- col-sm-6 col-xs-12 -->--}}
        {{--<div class="col-sm-6 col-xs-12 privacy_policy">--}}
          {{--<a href="contact-us.html">Contact us</a>--}}
          {{--<a href="privacy-policy.html">Privacy Policy</a>--}}
        {{--</div><!-- col-sm-6 col-xs-12 -->--}}
      {{--</div><!-- row clearfix -->--}}
    </div><!-- container -->
  </div><!-- footer -->
</footer>

</div>

<!-- JQUERY SCRIPTS -->
<script src="/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/flexslider/jquery.flexslider.js"></script>
<script src="/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="/plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
<script src="/plugins/pop-up/jquery.magnific-popup.js"></script>
<script src="/plugins/animation/waypoints.min.js"></script>
<script src="/plugins/count-up/jquery.counterup.js"></script>
<script src="/plugins/animation/wow.min.js"></script>
<script src="/plugins/animation/moment.min.js"></script>
<script src="/plugins/calender/fullcalendar.min.js"></script>
<script src="/plugins/owl-carousel/owl.carousel.js"></script>
<script src="/plugins/timer/jquery.syotimer.js"></script>
<script src="/plugins/smoothscroll/SmoothScroll.js"></script>
<script src="/js/custom.js"></script>

        <!--Start of Tawk.to Script-->
      <script type="text/javascript">
      var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
      (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/5c020aaffd65052a5c93483c/default';
      s1.charset='UTF-8';
      s1.setAttribute('crossorigin','*');
      s0.parentNode.insertBefore(s1,s0);
      })();
      </script>
      <!--End of Tawk.to Script-->
</body>
</html>

