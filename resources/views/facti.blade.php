@include('header')
<!-- ERROR CONTENT -->
<div class="mainContent clearfix">
    <div class="single_banner">
        <div class="container">
            <div class="single_banner_inner" style="min-height: 200px; position: relative">
                <img src="/img/banner-interno-facti.jpg" alt=""/>
            </div><!--end single_banner_inner-->
        </div><!--end container-->
    </div><!--end single banner-->
    <div class="container">
        <h2><br>SOBRE A FACTI<br><br></h2>

        <p><b>A Faculdade de Tecnologia Ícone (FACTI)</b> é comprometida com a valorização do desenvolvimento humano, científico e tecnológico. Propõe educação continuada, inclusiva e integrada de graduação, pós-graduação, EAD e presencial, e professores altamente qualificados, que atuam nas diversas esferas educacionais, o que possibilita uma interação entre a teoria e a prática, com o objetivo de agregar experiências. Ensejamos oportunizar uma educação contemporânea de qualidade, em tempo hábil, possibilitando o ingresso de novos profissionais no mercado de trabalho, em conformidade com as leis de diretrizes educacionais.</p>

        <p>Uma das prioridades da <b>Faculdade de Tecnologia (FACTI)</b> é a <b>Política de atendimento às pessoas com Deficiência e Necessidades Educacionais (PNE)</b> e a integração da pessoa com deficiência, desde limitação física, intelectual ou dificuldades de aprendizagem. Preocupa-se em proporcionar acessibilidade às pessoas com mobilidade reduzida (permanente ou temporária) e à pessoa com deficiência, que apresente completo ou parcial comprometimento de suas capacidades motoras, visuais, auditivas ou quaisquer outras que necessitem de auxílio na busca por condições igualitárias, bem como aos portadores do Transtorno do Espectro Autista (TEA). 
        As instalações da faculdade foram projetadas em conformidade com a Lei de Acessibilidade - Decreto nº 5.296 de 2 de dezembro de 2004 e suas diretrizes.</p>

        <hr>

        <h3>MISSÃO</h3>
        <p>Contribuir na solução de problemas educacionais e proporcionar valores acadêmicos, a fim de promover a profissionalização e treinamentos teórico-práticos às pessoas físicas e jurídicas.
        </p>

        <hr>

        <h3>VISÃO</h3>
        <p>Ser reconhecida como instituição de ensino de qualidade e eficiência, a partir de sua vocação principal que é a formação acadêmica e profissional, promovendo valores como: ética, profissionalismo, compromisso e união.</p>

        <hr>

        <h3>VALORES</h3>
        <ul>
            <li><b>I -</b> Crença em Deus;</li>
            <li><b>II -</b> Ética;</li>
            <li><b>III -</b> Princípios de dignidade humana;</li>
            <li><b>IV -</b> Valorização das diferenças e das diversidades;</li>
            <li><b>V -</b> Respeito ao meio ambiente.</li>
        </ul>

        <hr>

        <h3>OBJETIVOS INSTITUCIONAIS</h3>
        <ul>
            <li><b>I -</b> Qualificação de profissionais em nível superior;</li>
            <li><b>II -</b> Realização de pesquisa e o estímulo às atividades criadoras;</li>
            <li><b>III -</b> Promover do intercâmbio e da cooperação com instituições de ensino dos diferentes níveis, bem assim com entidades de serviços, tendo em vista o desenvolvimento da cultura, das artes, das ciências e da tecnologia;</li>
            <li><b>IV -</b> Educar para a valorização individual do cidadão, sua adaptação social, bem como para o desenvolvimento do pensamento reflexivo;</li>
            <li><b>V -</b> Torna-se excelência em cursos tecnológicos.</li>
        </ul>

        <hr>

        <h3>HISTÓRICO E DESENVOLVIMENTO DA INSTITUIÇÃO</h3>
        <p>A <b>Faculdade de Tecnologia Ícone (FACTI)</b> iniciou a constituição do seu Projeto de Desenvolvimento Institucional em 2015 sendo credenciada pela <b>Portaria MEC nº 1.608, de 28 de dezembro de 2017, publicada no DOU de 29 de dezembro de 2017.</b></p>
        
        <p><b>A Faculdade de Tecnologia Ícone (FACTI)</b> é uma instituição nova e inovadora. Nova no que diz respeito à sua existência jurídica. Inovadora porque alia três pilares básicos de estratégia educacional em um ambiente de alta tecnologia e de acesso a todos os alunos e membros da comunidade onde está inserida: planejamento, acompanhamento e avaliação de resultados.</p>

        <p>Desde a fundação, o Ícone Instituto possuiu como atividade substantiva o ensino de cursos livres de formação continuada, consultoria em projetos sociais e atendimento psicopedagógico. Além disso, desenvolveu projetos, programas e eventos que visam o aperfeiçoamento e a qualificação de profissionais da área de educação, de gestão, de recursos humanos e do meio ambiente, atendendo à demanda da cidade.</p>

        <p>Todos os cursos da instituição são ministrados por profissionais qualificados e com sólida formação acadêmica. Destacam-se os cursos de formação continuada para professores que se apresentaram como uma demanda expressiva no mercado educacional.</p>

        <p>A Faculdade entende que ações de avaliação, em suas várias formas, visam sempre à melhoria da qualidade da formação, da extensão e da produção do conhecimento, além de demonstrarem seu desempenho significativo no tocante à responsabilidade social.</p>

        <p>O objetivo geral é acompanhar e aperfeiçoar o Projeto Pedagógico Institucional, promovendo a permanente melhoria e pertinência das atividades relacionadas a ensino, pesquisa, extensão e gestão.</p>

        <hr>
   


    </div>
</div>
@include('footer')

