@include('header')

<div style="width: 50%;margin: 0 auto;">
	<div style="background-color: #fff; display: flex;justify-content: center; flex-wrap: wrap; margin-top: 10%; margin-bottom: 10%;">	
		<h1 style="color: #7F0016; font-family: sans-serif; width: 100%; margin:0 auto; text-align: center;" >Parabéns pela iniciativa de investir em você.<br>
		Em breve entraremos em contato com todas as informações do curso de sua escolha!</h1>
		<a href="javascript:void(0)" onClick="history.go(-1); return false;"> 
			<button style="background-color: #34C839;width: 100%;height: auto;border: none;color: #fff;font-size: 1.6rem;border-radius: 7px;margin-top: 19px;font-family: 'Montserrat', sans-serif;font-weight: 600;cursor: pointer;
    			text-align: center;padding-top: 15px;padding-bottom: 15px;padding-right: 20px;
    			padding-left: 20px;">
				Ok, Voltar
			</button>
		</a>
	</div>
</div>

@include('footer')