@include('header')


    <!-- MAIN CONTENT -->
    <div class="main-content">

      <div class="banner-bottom">
        <form action="/cursos" method="get">
          <div class="container">
            <div class="banner-bottom-inner" style="position: static; z-index: auto; float: none; margin-top: 10px;">
              <div class="row">
                <div class="col-sm-4">
                  <div class="selectBox select-category clearfix">
                    <select name="id_categoria">
                      <option value="0"><i class="fa fa-align-justify" aria-hidden="true"></i>Todas Categorias</option>
                      @foreach($categorias as $categoria)
                        <option value="{{ $categoria['id_categoria'] }}">{{ $categoria['st_categoria'] }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-8">
                  <div class="searchCourse">

                    <input type="text" placeholder="Buscar pelo meu curso..." name="st_texto" class="form-control">
                    <button class="btn btn-default commonBtn" type="submit">Buscar</button>

                  </div>
                </div>

              </div>
            </div>
          </div>
        </form>
      </div>

      <div class="container">

        <!--POPULAR COURSE -->
        <div class="popularCourse padding clearfix">

          @if(!empty($categorias))
          @foreach($categorias as $categoria)
              <h3> {{ $categoria['st_nomeexibicao'] }}</h3>
            @foreach($categoria['produtos'] as $produto)
              @if ($loop->first) <div class="row"> @endif
            <div class="col-md-3 col-sm-6 col-xs-6">
              <div class="imageBox">
                <div class="productImage clearfix">
                  <a href="/curso/{{ $produto['id_projetopedagogico'] }}/{{ mb_strtolower($produto['st_produto'], 'UTF-8') }}"><img src="/img/home/course/course-img1.jpg" alt="Image"></a>
                  <span class="sticker" style="border: 1px solid lightgray;">
                    {{--<i class="fa fa-clock-o" aria-hidden="true"></i>--}}
                    {{ $produto['st_nomeexibicao'] }}
                  </span>
                </div>
                <div class="productCaption clearfix">
                  <div style="padding: 5px; min-height: 150px">
                  <h3 style="margin: 5px; padding: 0px"><a href="/curso/{{ $produto['id_projetopedagogico'] }}/{{ mb_strtolower($produto['st_produto'], 'UTF-8') }}">{{ $produto['st_produto'] }}</a></h3>
                  <div class="product-meta" style="font-size: 12px">
                    @if(trim(strip_tags($produto['st_descricao'])) != '')
                    {!! substr(strip_tags($produto['st_descricao']), 0, 120) !!}...
                    @endif
                    &nbsp;<a href="/curso/{{ $produto['id_projetopedagogico'] }}/{{ mb_strtolower($produto['st_produto'], 'UTF-8') }}">Saiba mais!</a>
                  </div>
                  </div>
                  {{--<div class="rating">--}}
                    {{--<span class="rating-star">--}}
                      {{--<i class="fa fa-star" aria-hidden="true"></i>--}}
                      {{--<i class="fa fa-star" aria-hidden="true"></i>--}}
                      {{--<i class="fa fa-star" aria-hidden="true"></i>--}}
                      {{--<i class="fa fa-star" aria-hidden="true"></i>--}}
                      {{--<i class="fa fa-star-o" aria-hidden="true"></i>--}}
                    {{--</span>--}}
                    {{--<span class="rating-review">(20 Reviews)</span>--}}
                  {{--</div>--}}
                  <div class="caption-bottom">
                    <div class="price">
                      <span>R$ {{ $produto['nu_valormensal'] }} mensais</span>
                      {{--<span class="regular-price"><del>$100</del></span>--}}
                    </div>
                    <div class="user" style="text-align: center; padding: 10px">
                      <a class="btn btn-success btn-matricule-se" href="/curso/{{ $produto['id_projetopedagogico'] }}/matricular" role="button">Matricule-se</a>
                    </div>
                  </div>
                </div>
              </div>
              </div>
                @if ($loop->last) </div> @endif
              @endforeach
            @endforeach
            @else
            Ops, não encontramos cursos com os parâmetros informados
            @endif

        </div>

      </div>
    </div>

@include('footer')
