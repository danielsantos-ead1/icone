<?php

namespace App\Http\Controllers\Web;

use App\Utils\G2;

class IndexController extends BaseController
{
    public function index()
    {
        $gestor = new G2();
        $categorias = $gestor->listarCategorias();
        if(!$categorias){
            $categorias = array();
        }

        $produtos = array();
        foreach ($categorias as $categoria){
            if(!empty($categoria['produtos'])) $produtos = array_merge($produtos, $categoria['produtos']);
        }




//        echo '<pre>'.__FILE__.'('.__LINE__.')';
//        var_dump($newProdutos);
//        exit;
//
//        $produtos = $gestor->listarProdutos();
        if(!$produtos){
            $produtos = array();
        }
        shuffle($produtos);
        $newProdutos = array();
        foreach ($produtos as $chave => $valor) {
            if($chave <= 1) $newProdutos[] = $valor;
        }

        return view('index', [
            'slider' => [
                'items' => [
                    [
                        'title' => null,
                        'image' => '/img/home/slider/banner-3.jpg',
                        'description' => null,
                        'url_text' => null,
                        'url' => '/cursos'
                    ],
                    [
                        'title' => 'UCAM',
                        'image' => '/img/home/slider/banner-4.jpg',
                        'description' => null,
                        'url_text' => null,
                        'url' => '/cursos'
                    ]
                    
                ]
            ],
            'categorias' => $categorias,
            'produtos' => $newProdutos
        ]);



    }

    public function facti(){
        return view('facti');
    }

}
