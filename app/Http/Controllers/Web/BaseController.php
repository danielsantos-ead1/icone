<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Utils\G2;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{

    public $entity;

    public function __construct() {

        //die('Site em Manutenção'); // comentar esta linha para editar

        $clearCache = Input::get('clear_cache');
        $gestor = new G2($clearCache);

        $entidade = $gestor->retornarEntidade();
        $dadosFixos = [
            'name'=>'Instituto Icone',
            'phone'=>'',
            'phones'=>['61 4141.2662'],
            'whats' =>'61 99626.8305',
            'email'=>'contato@facti.com.br',
            'logo'=>'logo-facti3.png',
            'checkout_url'=>'https://loja.ead1.net/loja/pagamento/iniciar',
//            'checkout_url'=>'http://dev2loja.emgsoft.com.br/loja/pagamento/iniciar',
            'st_chave'=>$gestor->getSt_chave(),
            'address'=> [
                'street'=>'',
                'extra'=>'',
                'number'=>'',
                'zipcode'=>'',
                'district'=>'',
                'city'=>'',
                'state'=>''
            ],
            'address2'=> [
                'street'=>'',
                'extra'=>'',
                'number'=>'',
                'zipcode'=>'',
                'district'=>'',
                'city'=>'',
                'state'=>''
            ],
            'social'=> [
                'twitter' => '',
                'facebook' => 'https://www.facebook.com/iconegrupoeducacional/',
                'instagram' => 'https://www.instagram.com/iconegrupoeducacional/',
                'gplus' => null,
                'youtube' => null
            ]
        ];

        if($entidade) {

            if(!empty($entidade['st_nomeentidade'])) $dadosFixos['name'] = $entidade['st_nomeentidade'];
            if(!empty($entidade['st_endereco']))     $dadosFixos['address']['street'] = $entidade['st_endereco'];
            if(!empty($entidade['st_complemento']) && !is_array($entidade['st_complemento']))     $dadosFixos['address']['extra'] = $entidade['st_complemento'];
            if(!empty($entidade['nu_numero']))     $dadosFixos['address']['number'] = $entidade['nu_numero'];
            if(!empty($entidade['st_nomemunicipio']))     $dadosFixos['address']['city'] = $entidade['st_nomemunicipio'];
            if(!empty($entidade['sg_uf']))     $dadosFixos['address']['state'] = $entidade['sg_uf'];
            if(!empty($entidade['st_bairro']))     $dadosFixos['address']['district'] = $entidade['st_bairro'];
            if(!empty($entidade['st_cep']))     $dadosFixos['address']['zipcode'] = $entidade['st_cep'];

        }

        $this->entity = $dadosFixos;


        View::share ( 'site', $dadosFixos);

    }

}
