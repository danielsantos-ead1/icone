<?php

namespace App\Http\Controllers;

use Page;
use Sitemap;

class SitemapsController extends Controller
{
    public function pages()
    {
        $pages = Page::all();

        foreach ($pages as $page) {
            $tag = Sitemap::addTag(route('pages.show', $page), $page->updated_at, 'daily', '0.8');

            foreach ($page->images as $image) {
                $tag->addImage($image->url, $image->caption);
            }
        }

        return Sitemap::render();
    }
}